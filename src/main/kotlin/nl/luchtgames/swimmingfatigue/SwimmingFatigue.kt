/*
 * SwimmingFatigue
 * Copyright (C) 2020  Martijn Heil
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package nl.luchtgames.swimmingfatigue

import org.bukkit.ChatColor
import org.bukkit.GameMode
import org.bukkit.block.Biome
import org.bukkit.block.BlockFace
import org.bukkit.entity.Entity
import org.bukkit.entity.HumanEntity
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.PlayerDeathEvent
import org.bukkit.plugin.java.JavaPlugin
import java.util.*

class SwimmingFatigue : JavaPlugin() {
    private val swimmingState = HashMap<UUID, Int>()

    override fun onEnable() {
        saveDefaultConfig()
        val maxSwimmingDuration = this.config.getInt("maxSwimmingDuration")
        server.pluginManager.registerEvents(object : Listener {
            @EventHandler(ignoreCancelled = true)
            fun onPlayerDeath(e: PlayerDeathEvent) {
                if((swimmingState[e.entity.uniqueId] ?: 0) >= maxSwimmingDuration) {
                    e.deathMessage = e.entity.name + " drowned."
                }
            }
        }, this)

        server.scheduler.scheduleSyncRepeatingTask(this, {
            server.onlinePlayers.forEach {
                if(!it.isDead) {
                    val timeInLiquid = swimmingState[it.uniqueId] ?: 0
                    fun getMessage(time: Int) = if(time != 1) ChatColor.RED.toString() + "Pas op! Als je over $time seconden nog niet uit het water bent, verdrink je!"
                    else ChatColor.RED.toString() + "Pas op! Als je over $time seconden nog niet uit het water bent, verdrink je!"

                    if(isSwimming(it)) {

                        if (timeInLiquid >= maxSwimmingDuration) {
                            it.sendMessage(ChatColor.DARK_RED.toString() + "Je bent verdronken!")
                            it.health = 0.0
                        } else {
                            swimmingState[it.uniqueId] = timeInLiquid + 1

                            when (timeInLiquid) {
                                maxSwimmingDuration - 5 -> it.sendMessage(getMessage(5))
                                maxSwimmingDuration - 10 -> it.sendMessage(getMessage(10))
                                maxSwimmingDuration - 30 -> it.sendMessage(getMessage(30))
                            }
                        }
                    } else {
                        swimmingState.remove(it.uniqueId)
                    }
                }
            }
        }, 0, 20)
    }

    override fun onDisable() {
        swimmingState.clear()
    }

    private fun isSwimming(it: Entity): Boolean {
        val b = it.location.block
        val biome = b.biome
        return (it is HumanEntity && !(it.gameMode == GameMode.SPECTATOR || it.gameMode == GameMode.CREATIVE) && !it.isInvulnerable &&
                b.isLiquid && (b.getRelative(BlockFace.DOWN).isLiquid || b.getRelative(BlockFace.UP).isLiquid))
    }
}
